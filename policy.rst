=====================================
 Debian Python Modules Team - Policy
=====================================

:Author: Gustavo Franco <stratus@debian.org>, Raphaël Hertzog <hertzog@debian.org>, Barry Warsaw <barry@debian.org>
:License: GNU GPL v2 or later

:Introduction:
  The Debian Python Modules Team (DPMT) aims to improve the Python modules
  situation in Debian, by packaging available modules that may be useful and
  providing a central location for packages maintained by a team, hence
  improving responsiveness, integration, and standardization.

  The DPMT is hosted at salsa.debian.org, the Debian GitLab installation. We
  currently have a Git repository and a mailing list whose email address can
  be used in the ``Maintainer`` or ``Uploaders`` fields on co-maintained
  packages.

  For more information send a message to: debian-python@lists.debian.org

.. contents::


Joining the team
================

The team is open to any Python-related package maintainer. To be added to
the team, please send your request to debian-python@lists.debian.org. Include
the following in your request:

* Why you want to join the team: e.g. maintain your current packages within
  the team, help maintain some specific packages, etc.
* Your Salsa login.
* A statement that you have read
  https://salsa.debian.org/python-team/tools/python-modules/blob/master/policy.rst
  and that you accept it.

The team accepts all contributors and is not restricted to Debian developers.
Several Debian developers of the team will gladly sponsor packages of non-DD
who are part of the team. Sponsorship requests can be sent on the main
discussion list or on #debian-python IRC channel (OFTC network).

All team members should of course follow the main discussion list:
debian-python@lists.debian.org


Maintainership
==============

A package maintained within the team should have the name of the team either
in the ``Maintainer`` field or in the ``Uploaders`` field.

Maintainer: Debian Python Modules Team <python-modules-team@lists.alioth.debian.org>

This enables the team to have an overview of its packages on the DDPO_website_.

* Team in ``Maintainers`` is a strong statement that fully collaborative
  maintenance is preferred. Anyone can commit to the Git repository and upload
  as needed. A courtesy email to ``Uploaders`` can be nice but not required.

* Team in ``Uploaders`` is a weak statement of collaboration. Help in
  maintaining the package is appreciated, commits to the Git repository are
  freely welcomed, but before uploading, please contact the ``Maintainer`` for
  the green light.

Team members who have broad interest should subscribe to the mailing list
python-modules-team@lists.alioth.debian.org whereas members who are only
interested in some packages should use the Package Tracking System to
follow the packages.


Git Procedures
==============

At the time of writing, the DPMT uses ``git-buildpackage`` to maintain the
packaging, which inherently uses Git for version control.

Previous DPMT policy requires ``git-dpm`` to manage patches as well as
packaging. However, we reverted the choice and use ``quilt`` to manage the
patches.  Optionally, we recommend you use ``gbp pq`` to manage patches.

Git repositories live on Salsa under the url
``git@salsa.debian.org:python-team/modules/<src-pkg.name>.git``. To access any
source package's repository, use ``gbp clone`` on the above url, substituting
*src-pkg-name* for the source package name of the package in question. You can
also access the GitLab web interface at
``https://salsa.debian.org/python-team/modules/<src-pkg-name>``.

DPMT requires a pristine-tar branch, and only upstream tarballs can be used to
advance the upstream branch. Complete upstream Git history should be avoided
in the upstream branch.

Unless concensus is reached in the DPMT and documented in the
`wiki <https://wiki.debian.org/Python/GitPackaging>`__ , deviations from this
policy are discouraged, in order to let team members being able to maintain
your packages.  When you have to deviate, you should include a
``debian/README.source`` file explaining the rationale for the deviation and
the details of working with the package's Git repository.


Branch names
------------

Currently, we strongly recommend using DEP-14_ branch naming scheme in the Git
repository, specifically:

* ``debian/master`` - The Debianized upstream source directory. In other words,
  this contains both the upstream source and a ``debian/`` packaging directory.
  It is a *source-full, patches-unapplied* checkout.
* ``pristine-tar`` - Contains the standard ``pristine-tar`` deltas.
* ``upstream`` - The un-Debianized upstream source. This is what you get when
  you unpack the upstream tarball. This could also be ``upstream/<version>`` if
  required.

If you have to use other branch names, you **MUST** document the differences
and explain the reason in ``debian/README.source``.  This includes security
branch, or downstream distributions packaging branch.  Please avoid using other
naming schemes than DEP-14_ unless you absolutely need to.

For sending merge requests in Salsa, please create a fork in your personal
account, instead of creating a branch in the team repository.

``gbp pq`` creates a ``patch`` branch when you want to directly edit upstream
source files for exporting to ``quilt`` patches.  However, ``patch`` branch is
for local use only, and should not be pushed to remote servers.

Configurations
--------------

To make sure per-user configurations do not violate the policy, having the
following configurations in ``debian/gbp.conf`` is recommended::

  [DEFAULT]
  pristine-tar = True
  upstream-branch = upstream
  debian-branch = debian/master
  upstream-tag = upstream/%(version)s
  debian-tag = debian/%(version)s

More information
----------------

Additional information for working with DPMT Git repositories, creating new
repositories, common workflows, and helpful hints, can be found on the
`Debian wiki <https://wiki.debian.org/Python/GitPackaging>`__
page.


Quality Assurance
=================

The goal of the team is to maintain all packages as best as possible.
Thus every member is encouraged to do general QA work on all the
packages: fix bugs, test packages, improve them to use the latest python
packaging tools, etc.


License
=======

Copyright (c) 2005-2018 Debian Python Modules Team. All rights reserved.
This document is free software; you may redistribute it and/or modify
it under the same terms as GNU GPL v2 or later.

.. _DDPO_website: http://qa.debian.org/developer.php?login=python-modules-team@lists.alioth.debian.org
.. _DEP-14: http://dep.debian.net/deps/dep14/
